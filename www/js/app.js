String.prototype.firstToUpper = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

angular.module('underscore', [])
    .factory('_', function() {
        return window._; // assumes underscore has already been loaded on the page
    });

angular.module('your_app_name', [
    'ionic',
    'ionic.native',
    'your_app_name.common.directives',
    'your_app_name.app.controllers',
    'your_app_name.app.services',
    'your_app_name.app.filters',
    'your_app_name.app.constants',
    'your_app_name.views',
    'underscore',
    'angularMoment',
    'ngIOS9UIWebViewPatch',
    'ionic-cache-src',
    'yaru22.angular-timeago',
    'ionic-native-transitions',
    'angular-loading-bar',
    'ngAnimate',
    'IonicAlternativeRefresher',
    'angular-jwt',
    'angular-md5',
    'ngPhotoswipe'
])

// Enable native scrolls for Android platform only,
// as you see, we're disabling jsScrolling to achieve this.
.config(function($ionicConfigProvider) {

    if (ionic.Platform.isAndroid()) {
        $ionicConfigProvider.scrolling.jsScrolling(false);
        $ionicConfigProvider.backButton.text('').icon('ion-android-arrow-back');
        $ionicConfigProvider.tabs
            .style('standard')
            .position('bottom');
        $ionicConfigProvider.views.transition('none');

    }

    $ionicConfigProvider.views.swipeBackEnabled(true);

})

.config(function($ionicNativeTransitionsProvider) {
    if (ionic.Platform.isAndroid()) {
        $ionicNativeTransitionsProvider.setDefaultTransition({
            type: 'slide',
            direction: 'left'
        })

        .setDefaultBackTransition({
            type: 'slide',
            direction: 'right'
        });
    }
})

.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('AuthInjector');
}])

.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
}])

.config(['$compileProvider', function($compileProvider) {
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|cdvphotolibrary|file|assets-library|cdvfile|content):/);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|cdvphotolibrary|file|assets-library|cdvfile):/);
    // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
}])

.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'https**',
        'http**'
    ]);
})

.config(function($ionicNativeTransitionsProvider) {
    $ionicNativeTransitionsProvider.setDefaultOptions({
        "duration": 300
    });
})

.config(function($stateProvider, $urlRouterProvider, TAB_STATES, TABS) {

    angular.forEach(TAB_STATES.parent, function(item) { //definiujemy stejty nadrzedne (tabow)

        $stateProvider.state('app.' + item.name, {
            url: item.url,
            abstract: item.abstract,
            views: item.views,
            nativeTransitions: item.nativeTransitions
        });
        //console.log('Parent state: ' + item.name);
    });

    angular.forEach(TABS, function(childrenObj, parent) { // definiujemy stejty mozliwe w ramach tabow

        angular.forEach(childrenObj.children, function(currentChild) {

            //console.log(TAB_STATES.child[currentChild].url)
            var viewName = 'app-' + childrenObj.name;
            var stateData = {
                url: TAB_STATES.child[currentChild].url,
                abstract: TAB_STATES.child[currentChild].abstract,
                views: {}
            };
            stateData.views['app-' + childrenObj.name] = {
                templateUrl: TAB_STATES.child[currentChild].templateUrl,
                controller: TAB_STATES.child[currentChild].controller,
            };
            $stateProvider.state('app.' + childrenObj.name + currentChild.firstToUpper(), stateData);

            //console.log('substate: app.' + childrenObj.name + currentChild.firstToUpper() );
            //jesli jest abstract to sprawdzamy i definiujemy kolejne stejty
            if (stateData.abstract === true) {
                angular.forEach(TAB_STATES.child[currentChild].substates, function(substate) {
                    var substateData = {
                        url: substate.url,
                        views: substate.views
                    };

                    $stateProvider.state('app.' + childrenObj.name + currentChild.firstToUpper() + '.' + substate.name, substateData);
                    //console.log('state: app.' + childrenObj.name + currentChild.firstToUpper() + '.' + substate.name);
                });
            }
        });
    });
    $stateProvider
    //SIDE MENU ROUTES
        .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "views/app/side-menu.html",
        controller: 'AppCtrl'
    })

    .state('app.add', {

        url: '/add',
        views: {
            'app-add': {
                controller: 'AddCtrl',

                templateUrl: 'views/app/add/step1.html'
            }
        }
    })

    .state('app.add2', {

        url: '/add',
        params: {
            photo: null
        },
        views: {
            'app-add': {
                controller: 'AddCtrl2',
                templateUrl: 'views/app/add/step2.html'
            }
        }
    })

    .state('app.add3', {

        url: '/add',
        params: {
            photo: null
        },
        views: {
            'app-add': {
                controller: 'AddCtrl3',
                templateUrl: 'views/app/add/step3.html'
            }
        }
    })



    .state('app.explore', {
        abstract: false,
        url: '/explore',
        views: {
            'app-explore': {
                templateUrl: "views/app/explore.html",
                controller: 'ExploreCtrl'
            }
        }
    })

    .state('app.settings', {
        url: "/settings",
        views: {
            'app-myProfile': {
                templateUrl: "views/app/profile/settings.html",
                controller: 'SettingsCtrl'
            }
        }
    })

    .state('app.shop', {
        url: "/shop",
        abstract: true,
        views: {
            'app-shop': {
                templateUrl: "views/app/shop/shop.html",
                controller: "ExploreCtrl"
            }
        }
    })

    .state('app.shop.home', {
        url: "/",
        views: {
            'shop-home': {
                templateUrl: "views/app/shop/shop-home.html",
                controller: 'ShopCtrl'
            }
        }
    })

    .state('app.shop.popular', {
        url: "/popular",
        views: {
            'shop-popular': {
                templateUrl: "views/app/shop/shop-popular.html",
                controller: 'ShopCtrl'
            }
        }
    })

    .state('app.shop.sale', {
        url: "/sale",
        views: {
            'shop-sale': {
                templateUrl: "views/app/shop/shop-sale.html",
                controller: 'ShopCtrl'
            }
        }
    })

    .state('app.cart', {
        url: "/cart",
        views: {
            'menuContent': {
                templateUrl: "views/app/shop/cart.html",
                controller: 'ShoppingCartCtrl'
            }
        }
    })

    .state('app.shipping-address', {
        url: "/shipping-address",
        views: {
            'menuContent': {
                templateUrl: "views/app/shop/shipping-address.html",
                controller: "CheckoutCtrl"
            }
        }
    })

    .state('app.checkout', {
        url: "/checkout",
        views: {
            'menuContent': {
                templateUrl: "views/app/shop/checkout.html",
                controller: "CheckoutCtrl"
            }
        }
    })

    .state('app.product-detail', {
        url: "/product/:productId",
        views: {
            'menuContent': {
                templateUrl: "views/app/shop/product-detail.html",
                controller: 'ProductCtrl'
            }
        }
    })

    //AUTH ROUTES
    .state('auth', {
        url: "/auth",
        templateUrl: "views/auth/auth.html",
        controller: "AuthCtrl",
        abstract: true
    })

    .state('auth.welcome', {
        url: '/welcome',
        templateUrl: "views/auth/welcome.html",
        controller: 'WelcomeCtrl',
        resolve: {
            show_hidden_actions: function() {
                return false;
            }
        }
    })

    .state('auth.login', {
        url: '/login',
        templateUrl: "views/auth/login.html",
        controller: 'LogInCtrl'
    })

    .state('auth.signup', {
        url: '/signup',
        templateUrl: "views/auth/signup.html",
        controller: 'SignUpCtrl'
    })

    .state('auth.forgot-password', {
        url: '/forgot-password',
        templateUrl: "views/auth/forgot-password.html",
        controller: 'ForgotPasswordCtrl'
    })

    // .state('facebook-sign-in', {
    //   url: "/facebook-sign-in",
    //   templateUrl: "views/auth/facebook-sign-in.html",
    //   controller: 'WelcomeCtrl'
    // })
    //
    // .state('dont-have-facebook', {
    //   url: "/dont-have-facebook",
    //   templateUrl: "views/auth/dont-have-facebook.html",
    //   controller: 'WelcomeCtrl'
    // })
    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/auth/welcome');
    // $urlRouterProvider.otherwise('/app/feed');
})

.config(function($cordovaInAppBrowserProvider) {

    var defaultOptions = {
        location: 'yes',
        clearcache: 'no',
        toolbar: 'yes'
    };
    document.addEventListener("deviceready", function() {

        $cordovaInAppBrowserProvider.setDefaultOptions(defaultOptions);

    }, false);
})

.run(function($ionicPlatform, $rootScope, $ionicHistory, $state, $timeout, $ionicConfig) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });

    // This fixes transitions for transparent background views
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
        if (toState.name.indexOf('auth.welcome') > -1) {
            // set transitions to android to avoid weird visual effect in the walkthrough transitions
            $timeout(function() {
                $ionicConfig.views.transition('android');
                $ionicConfig.views.swipeBackEnabled(true);
                //console.log("setting transition to android and disabling swipe back");
            }, 0);
        }

    });

    $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
        if (toState.name.indexOf('app.feed') > -1) {
            // Restore platform default transition. We are just hardcoding android transitions to auth views.
            $ionicConfig.views.transition('platform');
            // If it's ios, then enable swipe back again
            if (ionic.Platform.isIOS()) {
                $ionicConfig.views.swipeBackEnabled(true);
            }
            //console.log("enabling swipe back and restoring transition to platform default", $ionicConfig.views.transition());
        }
    });

    console.log("app run");
});