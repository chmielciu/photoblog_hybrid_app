angular.module('your_app_name.app.services')
    .service('FollowService', function($http, $q, $timeout, HOST, $rootScope) {

        return {

            followUser: function(uidToFollow) {
                return $q(function(resolve, reject) {

                    var payload = {
                        "friend_uid": uidToFollow
                    };
                    $http({
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: HOST + 'friend/add',
                        data: payload
                    }).then(
                        function(success) {
                            resolve(success);

                        },
                        function(error) {
                            reject(error);
                        }
                    );
                });
            },
            unfollowUser: function(uidToUnfollow) {
                return $q(function(resolve, reject) {

                    var payload = {
                        "friend_uid": uidToUnfollow
                    };
                    $http({
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        url: HOST + 'friend/remove',
                        data: payload
                    }).then(
                        function(success) {
                            resolve(success);

                        },
                        function(error) {
                            reject(error);
                        }
                    );
                });
            }
        };

    });
