angular.module('your_app_name.app.services')
.factory('UserService', function($localStorage, jwtHelper) {
    var user = {};

    return {
        /**
         * Initialize the user object by the means of a silent login.
         */
        silentLogin: function() {
            this._fromStorage();
        },

        /**
         * Set the user object.
         */
        setUser: function(data) {
            this._store(data);
        },

        /**
         * Get the user object, if not present -> populate
         * from localStorage
         */
        getUser: function() {
            return user;
        },

        /**
         * Set the facebook user object.
         */
        setFacebookUser: function(data) {
            $localStorage.facebook = data;
        },

        /**
         * Get the facebook user object
         */
        getFacebookUser: function() {
            if ( $localStorage.facebook ) {
                return $localStorage.facebook;
            }

            return false;
        },

        /**
         * Get the user uid
         * from localStorage
         */
        getUserName: function() {
            if ( user.uid ) {
                return user.uid;
            }

            return false;
        },

        /**
         * Get user id
         */
        getUserId: function() {
            if ( user.id ) {
                return user.id;
            }

            return false;
        },

        /**
         * Get password hash
         * from localStorage
         */
        getPassword: function() {
            if ( $localStorage.password ) {
                return $localStorage.password;
            }

            return false;
        },

        /**
         * Save password hash
         * to localstorage
         */
        setPassword: function(password) {
            $localStorage.password = password;
        },

        /**
         * Is user logged in.
         */
        isLogged: function() {
            return user.id ? true : false;
        },

        /**
         * Is user facebook logged in.
         */
        isLoggedByFacebook: function() {
            return (user.id && !$localStorage.password && $localStorage.facebook) ? true : false;
        },

        /**
         * Set the user authentication token.
         */
        setAuthToken: function(token) {
            var payload = jwtHelper.decodeToken(token);
            if ( payload ) {
                payload.auth_token = token;
                this.setUser(payload);
            }
        },

        /**
         * Get the user authentication token.
         */
        getAuthToken: function() {
            return user.auth_token;
        },

        /**
         * Is authentication token expired.
         */
        isAuthTokenExpired: function() {
            var auth_token = this.getAuthToken();
            if (auth_token) {
                return jwtHelper.isTokenExpired(this.getAuthToken());
            }
            
            return true;
        },

        /**
         * Remove session
         */
        removeSession: function() {
            $localStorage.user = null;
            $localStorage.password = null;
            $localStorage.feed = null;
            $localStorage.facebook = null;
            user = {};
        },

        /**
         * Populate the user object from $localStorage
         */
        _fromStorage: function() {
            if ( $localStorage.user && $localStorage.user.id 
                && $localStorage.user.auth_token ) {
                user = $localStorage.user;
            }
        },

        /**
         * Store the user object in $localStorage
         */
        _store: function(data) {
            if ( data.id && data.auth_token ) {
                $localStorage.user = data;
                this._fromStorage();
            }
        }
    }
});