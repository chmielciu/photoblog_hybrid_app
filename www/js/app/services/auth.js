angular.module('your_app_name.app.services')
.service('AuthService', function ($q, $http, $rootScope, HOST, UserService, ERROR_CODES) {

    /**
     * Zaloguj uzytkownika loginem i haslem
     */
    this.login = function (login, password) {
        return $q(function (resolve, reject) {
            $http.post(HOST + 'login', {"login": login, "password": password}).then(
                function(response) {
                    if (response.data.auth_token) {
                        console.log('login success');
                        // jesli udana to ustaw token, dodaj haslo do obiektu user do storage
                        UserService.setAuthToken(response.data.auth_token);
                        UserService.setPassword(password);

                        $rootScope.$broadcast('auth:login:success');
                        resolve(UserService.getUser());
                    } else {
                        reject("login error");
                        $rootScope.$broadcast('auth:login:failure');
                    }
                },
                function(error) {
                    var error_message = ERROR_CODES['ERR_GENERAL_LOGIN_ERROR'];

                    if ( error.data && error.data.code) {
                        error_message = ERROR_CODES[error.data.code];
                    }

                    reject(error_message);
                    $rootScope.$broadcast('auth:login:failure');
                }
            );
        });
    };

    /**
     * Zaloguj uzytkownika danymi z facebook
     */
    this.facebookLogin = function() {
        // Wyślij token na serwer, poczekaj na odbior tokena
        var facebookUser = UserService.getFacebookUser();

        return $q(function (resolve, reject) {
            $http.post(HOST + 'facebook-login', {
                "facebook_user_id": facebookUser.id,
                "facebook_auth_token": facebookUser.fb_auth_response.accessToken
            }).then(
                function(response) {
                    if (response.data.auth_token) {
                        console.log('login success');
                        // jesli udana to ustaw token, dodaj haslo do obiektu user do storage
                        UserService.setAuthToken(response.data.auth_token);

                        $rootScope.$broadcast('auth:login:success');
                        resolve(UserService.getUser());
                    } else {
                        reject("login error");
                        $rootScope.$broadcast('auth:login:failure');
                    }
                },
                function(error) {
                    var error_message = ERROR_CODES['ERR_GENERAL_LOGIN_ERROR'];

                    if ( error.data && error.data.code) {
                        if ( error.data.code === 'ERR_FACEBOOK_USER_MISSING' ) {
                            error_message = 'ERR_FACEBOOK_USER_MISSING';
                        } else {
                            error_message = ERROR_CODES[error.data.code];
                        }
                    }

                    reject(error_message);
                    $rootScope.$broadcast('auth:login:failure');
                }
            );
        });
    };

    /**
     * Akcja logowania facebook
     */
    this.facebookSignIn = function () {
        if ( typeof facebookConnectPlugin === "undefined" ) {
            return $q(function (resolve, reject) {
                reject("Brak plugin facebookConnectPlugin");
            });
        }

        return this.facebookGetLoginStatus()
            .then(function(success) {
                if (success.status === 'connected') {
                    return this.getFacebookProfileInfo(success.authResponse);
                } else {
                    return this.facebookConnectPluginLogin()
                        .then(this.getFacebookProfileInfo);
                }
            }.bind(this)).then(function (facebookUser) {
                return this.facebookLogin();
            }.bind(this));
    };

    this.facebookGetLoginStatus = function() {
        return $q(function (resolve, reject) {
            facebookConnectPlugin.getLoginStatus(function (success) {
                resolve(success);
            });
        });
    };

    this.facebookConnectPluginLogin = function() {
        return $q(function (resolve, reject) {
            facebookConnectPlugin.login(['email', 'user_birthday', 'user_location'], function (response) {
                if (!response.authResponse) {
                    reject("Cannot find the authResponse");
                    return;
                }

                resolve(response.authResponse);
            }, function (error) {
                console.log('facebookConnectPluginLogin', error);
                reject(error);
            });
        });
    };

	// This method is to get the user profile info from the facebook api
	this.getFacebookProfileInfo = function (authResponse) {
		var info = $q.defer();

		facebookConnectPlugin.api('/me?fields=email,name,birthday,location&access_token=' + authResponse.accessToken, null,
			function (response) {
                var facebookUser = response;

                facebookUser.fb_auth_response = authResponse;
                UserService.setFacebookUser(facebookUser);

				console.log(facebookUser);
				info.resolve(facebookUser);
			},
			function (response) {
				info.reject(response);
			}
		);

		return info.promise;
	};

    /**
     * Zarejestruj uzytkownika danymi z facebook
     */
    this.facebookRegister = function(login, password, facebookAuthToken) {
        // Wyślij token na serwer, poczekaj na odbior tokena
        // fotoblogowego
        return $q(function (resolve, reject) {
            $http.post(HOST + 'facebook-register', {
                "login": login, 
                "password": password,
                "facebook_auth_token": facebookAuthToken
            }).then(
                function(response) {
                    if (response.data.auth_token) {
                        console.log('register success');
                        // jesli udana to ustaw token, dodaj haslo do obiektu user do storage
                        UserService.setAuthToken(response.data.auth_token);

                        $rootScope.$broadcast('auth:register:success');
                        resolve(UserService.getUser());
                    } else {
                        reject(ERROR_CODES['ERR_GENERAL_REGISTER_ERROR']);
                        $rootScope.$broadcast('auth:register:failure');
                    }
                },
                function(error) {
                    var error_message = ERROR_CODES['ERR_GENERAL_REGISTER_ERROR'];

                    if ( error.data && error.data.code) {
                        error_message = ERROR_CODES[error.data.code];
                    }
                    
                    console.log(error);
                    console.log(error_message);

                    reject(error_message);
                    $rootScope.$broadcast('auth:register:failure');
                }
            );
        });
    };

    /**
     * Wyloguj uzytkownika
     */
    this.logout = function () {
        UserService.removeSession();

        $rootScope.$broadcast('auth:logout:success');
    };
});
