angular.module('your_app_name.app.services')
    .service('ModerationService', function($rootScope, $http, $q, $localStorage, HOST, $ionicSlideBoxDelegate) {

        this.getAwaitingModerate = function() {
            //console.log('wywoluje getAwaitingModerate');
            var dfd = $q.defer();
            var res;
            $http.get(HOST + 'comments/awaiting-moderate.json').then(

                function(response) {
                    var commentsByFid = {};
                    //poukladac tak aby kluczem byl fid
                    //console.log(response);

                    angular.forEach(response.data.comments.reverse(), function(el) {
                        !commentsByFid[el.fileid] && (commentsByFid[el.fileid] = [])
                        el.tomoderate = true;
                        commentsByFid[el.fileid].push(el);
                    });

                    allComments = {
                        commentsByFid: commentsByFid,
                        response: response.data.comments
                    };
                    dfd.resolve(allComments);
                },
                function(error) {

                    dfd.reject(error);
                    $rootScope.$broadcast('httpError', error);
                });
            return dfd.promise;
        };

        this.moderate = function(comment_id, method, filename) {
            return $q(function(resolve, reject) {

                $http({
                    method: method,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: HOST + 'comments/moderate/' + filename + '.json',
                    data: JSON.stringify({
                        comment_id: comment_id
                    })
                }).then(function(response) {
                        //console.log(response);
                        resolve({
                            status: response
                        });
                    },
                    function(response) {
                        //console.log(response);
                        reject({
                            status: response
                        });
                    }
                );

            });
        };


    })