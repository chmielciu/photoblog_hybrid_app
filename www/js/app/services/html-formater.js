angular.module('your_app_name.app.services')
    .service('HtmlFormaterService', function($compile, DOMAIN) {

        this.format = function(htmlString, tabName) {      
            var emptylines = ['<br(\\s)?\\/?>', '<p[^>]*>(&nbsp;|\\s)<\\/p>']; //usuwamy puste linie (zostawiamy jedna)
            emptylines.forEach(function(el) {
                var re = new RegExp('((\\s*(' + el + ')\\s*){2,})+', "gi");
                htmlString = htmlString.replace(re, '');
            });

            re = new RegExp('~@\\[([a-z0-9]+)\\]\\$~', 'gi'); // zamien tagi na linki
            htmlString = htmlString.replace(re,
                '<a ui-sref="app' + tabName + 'Profile.posts({userId:$1})" href="#/app/profile/$1/posts">$1</a>');
            //TODO: dorobić tagi. Pierwej trzeba ustalić karte przegladaj - ok przegladanie tagow tylko przez strone
            return htmlString;
        };

        this.prepareLinks = function(html){
          re = new RegExp('/(proxy.php[^\'|\"]+)', 'gi');
          html = html.replace(re, DOMAIN+'$1');
              return html.replace(/href="([^"']+)"/gm, "onclick=\"event.preventDefault(); window.open('$1', '_blank')\"");
        };
    });
