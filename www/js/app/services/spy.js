angular.module('your_app_name.app.services')
    .service('SpyService', function($rootScope, $http, $q, $localStorage, HOST) {

        this.getSpy = function() {
            var dfd = $q.defer();
            $http.get(HOST + 'spy.json?user_id=5').then(function(data) {
                var spyData = data.data.spy;
                dfd.resolve(spyData);
            }, function(error) {
                error.action = true;
                $rootScope.$broadcast('httpError', error);
                console.log(error);
                dfd.reject(error);
            });
            return dfd.promise;
        };
      });
