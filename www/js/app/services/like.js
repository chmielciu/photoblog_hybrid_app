angular.module('your_app_name.app.services')
.service('LikeService', function($http, $q, HOST) {

    this.like = function(photoId, method) {
        return $q(function(resolve, reject) {

            $http({
                method: method,
                headers: {
                    'Content-Type': 'application/json'
                },
                url: HOST + 'like.json',
                data: JSON.stringify({
                    photo_id: photoId
                })
            }).then(function(response) {
                    console.log(response);
                    resolve({
                        status: response
                    });
                },
                function(response) {
                    console.log(response);
                    reject({
                        status: response
                    });
                }
            );

        });
    };

});