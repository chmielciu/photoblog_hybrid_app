angular.module('your_app_name.app.services')
    .service('PasswordProtectionService', function($http, $q, $localStorage, $timeout, HOST, $rootScope) {

        this.saveToLocalStorage = function(pwd, uid) {
            return $q(function(resolve, reject) {
                if (!$localStorage.feed.blogPassword) {
                    $localStorage.blogPassword = {};
                }
                $localStorage.blogPassword[uid] = pwd;
                resolve('ok');
            });
        };
        this.checkPassword = function(pwd, uid) {
            return $q(function(resolve, reject) {
                $http({
                    method: 'POST',
                    url: HOST + 'blog/password.json',
                    data: {
                        'uid': uid,
                        'blog_password': pwd
                    }
                }).then(
                    function(res) {
                        resolve(res);
                    },
                    function(err) {
                        reject(err);
                    }
                );

            });

        };

        this.getLocalStoragePassword = function(uid) {
            return $q(function(resolve, reject) {
              console.log('getLStoragePass', uid);
              $localStorage.blogPassword = $localStorage.blogPassword ? $localStorage.blogPassword : {};
                if ($localStorage.blogPassword[uid]) {
                    resolve($localStorage.blogPassword[uid]);

                } else {
                  console.log('no pass in lstorage service')
                    reject('no password');

                }
            });
        };

    });
