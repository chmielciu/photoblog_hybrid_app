if (typeof(String.prototype.startsWith) === 'undefined') {
    String.prototype.startsWith = function(str) {
        return this.slice(0, str.length) === str;
    };
}

angular.module('your_app_name.app.services')
.factory('AuthInjector', function($q, $injector, HOST, UserService, jwtHelper) {

    var authInjector = {
        request: function(config) {
            console.log('AuthInjector request config.url: ' + config.url);
            if ( config.url.startsWith('views/') || config.url.startsWith(HOST+'views/')
                 || config.url == HOST + 'login' || config.url == HOST + 'facebook-login') {
                return config;
            }
            
            if ( UserService.isLogged() && !UserService.isAuthTokenExpired() ) {
                config.headers['Authorization'] = "Bearer " + UserService.getAuthToken();

                return config;
            } else if ( UserService.isLogged() ) {
                var AuthService = $injector.get('AuthService'),
                    login = UserService.getUserName(),
                    password = UserService.getPassword(),
                    facebookUser = UserService.getFacebookUser();
                
                return $q(function (resolve, reject) {
                    if ( facebookUser && !password ) {
                        AuthService.facebookSignIn().then(
                            function(response) {
                                console.log('AuthInjector facebookSignIn token regenerated');
                                config.headers['Authorization'] = "Bearer " + UserService.getAuthToken();

                                resolve(config);
                            },
                            function(reason) {
                                console.log('AuthInjector facebookSignIn token regeneration error');

                                resolve(config);
                            }
                        );
                    } else {
                        AuthService.login(login, password).then(
                            function(response){
                                console.log('AuthInjector token regenerated');
                                config.headers['Authorization'] = "Bearer " + UserService.getAuthToken();

                                resolve(config);
                            },
                            function(reason) {
                                console.log('AuthInjector token regeneration error');

                                resolve(config);
                            }
                        );
                    }
                });
            }

            return config;
        }
    };

    return authInjector;
});