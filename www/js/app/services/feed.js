angular.module('your_app_name.app.services')
.factory('FeedService', function($http, $q, $sce, $localStorage, $window, $timeout, HOST, $rootScope, HtmlFormaterService) {
    return {
        posts: [],
        sortedPosts: [],
        totalPages: 1,
        pageSize: 15,
        promise: function(page) {

            var self = this;
            return $q(function(resolve, reject) {
                  $http.get(HOST + 'feed.json').then(
                    function(database) {
                        console.log('get:');
                        console.log(database);
                        database = database.data;
                        pageSize = self.pageSize;
                        skip = pageSize * (page - 1);
                        totalPosts = database.posts.length;
                        self.totalPages = Math.ceil(totalPosts / pageSize);
                        var sortedPosts = _.sortBy(database.posts, function(post) {
                            return post.fid;
                        });
                        
                        var photoSwipeArray = [];
                        sortedPosts = _.each(sortedPosts, function(post) {
                            post.date = post.date * 1000;
                            if (($window.innerWidth * post.picture_height) / post.picture_width < 0.7 * $window.innerHeight) {
                                post.display_width = Math.floor($window.innerWidth);
                                post.display_height = Math.floor((post.display_width * post.picture_height) / post.picture_width);
                            } else {
                                post.display_height = Math.floor(0.7 * $window.innerHeight);
                                post.display_width = Math.floor((post.picture_width * post.display_height) / post.picture_height);
                            }
                            //    post.note_html = $sce.trustAsHtml(HtmlFormaterService.format(post.note_text));
                            //    post.note_html = $compile(post.note_html[0].outerHTML);
                            post.note_text = HtmlFormaterService.prepareLinks(post.note_text);
                            post.note_html = $sce.trustAsHtml(HtmlFormaterService.format(post.note_text));
                            return post;
                        });

                        postsToShow = sortedPosts.reverse().slice(skip, skip + pageSize);

                        //add user data to posts
                        var posts = _.each(postsToShow, function(post) {
                            return post;
                        });
                        
                        self.sortedPosts = sortedPosts;
                        console.log('posts from service');
                        console.log(sortedPosts);

                        resolve({
                            posts: posts,
                            totalPages: self.totalPages,
                            sortedPosts: sortedPosts,

                        });
                    },
                    function(error) {
                        error.action = true;
                        $rootScope.$broadcast('httpError', error);
                        console.log(error);
                        reject(error);
                    });
            });
        },
        getFeed: function(page) {
            var self = this;
            
            if (page === 1 && self.posts) {
             self.posts.length = 0;
            }

            if (page === 1 && $localStorage.feed) {
                for (var i = 0; i < $localStorage.feed.posts.length; i++) {
                    self.posts.push($localStorage.feed.posts[i]);
                    self.posts[i].note_html = $sce.trustAsHtml(HtmlFormaterService.format(self.posts[i].note_text, 'feed'));
                }

                self.totalPages = $localStorage.feed.totalPages;
                console.log('wziete z lokalstorydz');
            }

            self.promise(page).then(function(response) {
              console.log('response',response);
                var compareResponse = [];
                var compareSelfPosts = [];
                for (i = 0; i < response.posts.length; i++) {
                    compareResponse.push(response.posts[i].fid, response.posts[i].text, response.posts[i].liked);
                    if (self.posts[i])
                        compareSelfPosts.push(self.posts[i].fid, self.posts[i].text, self.posts[i].liked);
                }

                if (!angular.equals(compareResponse, compareSelfPosts)) {
                    console.log('isNoMatch');
                    self.posts.length = 0;
                    console.log(self.pageSize);
                    for (i = 0; i < response.posts.length; i++) {
                        self.posts[i] = response.posts[i];
                    }
                    response.posts = response.posts.slice(0,self.pageSize);
                    $localStorage.feed = response;
                    console.log('zapisalem do localstorage elementy', $localStorage.feed.posts.length);
                    //      console.log('localstorage feed po updacie: ' + $localStorage.feed);
                }
                $rootScope.$broadcast('scroll.refreshComplete');
                return response;
            }.bind(self));
        },

        updatePosts: function(page) {
          var self = this;
            return $q(function(resolve, reject) {

                var pageSize = self.pageSize,
                skip = pageSize * (page - 1),
                posts = [];

            posts = self.sortedPosts.slice(skip, skip + pageSize);

            for (var i=0; i < posts.length; i++) {
                self.posts.push(posts[i]);
                console.log('puszuje', self.posts.length, self.sortedPosts.length);
            }
            resolve(self.posts);
          });
        }
    };
});
