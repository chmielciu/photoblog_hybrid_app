angular.module('your_app_name.app.services')
    .service('PostService', function($rootScope, $http, $q, $sce, $localStorage, HOST, HtmlFormaterService) {

        this.photoId_comment = '';
        this.getPostComments = function(photoId, commentList) {
            this.photoId_comment;
            var dfd = $q.defer();
            var processComments = function(database) {
                var comments_list = [];
                // tworzymy liste komentarzy glownych
                angular.forEach(database.comments, function(com) {

                    if (!com.reply_crid) { //nie  odpowiedź
                        comments_list.push({
                            user: com.uid,
                            entry_photo: com.entry_photo,
                            commentator_nick: com.commentator_nick,
                            avatar: com.avatar,
                            toModerate: com.tomoderate,
                            text: com.comment_body,
                            cid: com.cid,
                            comment_time: com.comment_time,
                            sort_time: com.comment_time // czas pomocniczy do sortowania ng-repeat
                        });
                        // console.log('zwykly komentarz');
                    }
                });
                // szukamy odpowiedzi do komentarzy i komentarzy z odpowiedziami i dopisujemy je we wlascwie miejsca

                angular.forEach(database.comments, function(rply) {
                    if (rply.reply_crid) {
                        // dodajemy komentarz jesli trafimy na odpowiedz do nieistniejacego komentarza
                        // (w jsonie z pb nie ma komentarza glownego - sa same odpowiedzi)
                        // console.log('komentarz z odpowiedziami');
                        if (!_.findWhere(comments_list, {
                                "cid": rply.reply_cid
                            })) {
                            comments_list.push({
                                user: rply.uid,
                                commentator_nick: rply.commentator_nick,
                                text: rply.comment_body,
                                cid: rply.cid,
                                comment_time: rply.comment_time,
                                sort_time: rply.comment_time,
                                avatar: rply.avatar
                            });
                            // console.log('odpowiedź do komcia');
                        }
                        var rpl_cmnt_index = _.findIndex(comments_list, {
                            cid: rply.reply_cid
                        });

                        var reply_data = {
                            reply_cid: rply.reply_cid,
                            reply_text: rply.reply_comment_body,
                            reply_user: rply.reply_uid,
                            reply_commentator_nick: rply.reply_commentator_nick,
                            reply_comment_time: rply.reply_comment_time,
                            reply_toModerate: rply.reply_tomoderate,
                            reply_c: rply.reply_cid,
                            reply_avatar: rply.reply_avatar
                        };
                        reply_data.sort_time = rply.reply_comment_time;

                        if (!comments_list[rpl_cmnt_index].replies && !rply.reply_tomoderate)
                            comments_list[rpl_cmnt_index].replies = [];
                        if (!comments_list[rpl_cmnt_index].repliesToModerate && rply.reply_tomoderate)
                            comments_list[rpl_cmnt_index].repliesToModerate = [];
                        if (!rply.reply_tomoderate)
                            comments_list[rpl_cmnt_index].replies.push(reply_data);
                        else
                            comments_list[rpl_cmnt_index].repliesToModerate.push(reply_data);
                    }
                });
                //console.log(comments_list);
                dfd.resolve(comments_list);
            };

            if (!commentList) { // roznicujemy czy wpadamy tu z moderation czy wyswietlamy komentarze pod wpisem z post
                $http.get(HOST + 'comment.json?photo_id=' + photoId).then(
                    function(res) {
                        processComments(res.data);
                    });
            } else {
                var toProcess = {};
                toProcess.comments = commentList;
                processComments(toProcess);
            }

            return dfd.promise;
        };

        this.getCommentSettings = function(photoId) {
            var self = this;
            return $q(function(resolve, reject) {
                $http.get(HOST + 'comments/settings.json?photo_id=' + photoId).then(
                    function successCallback(response) {
                        resolve({
                            status: response.data.status,
                            permission: response.data.data.permission,
                            flag: response.data.data.flag
                        });
                    },
                    function unsucessfulCallback(response) {
                        reject(response);
                    }
                );
            });
        };

        this.putComment = function(photoId, comment, cid) {
            return $q(function(resolve, reject) {
                console.log(photoId);
                var payload;
                if (!cid) {
                    payload =
                        JSON.stringify({
                            photo_id: photoId,
                            comment: comment
                        });
                } else {
                    payload =
                        JSON.stringify({
                            photo_id: photoId,
                            comment: comment,
                            reply_to_id: cid
                        });
                }

                $http({
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: HOST + 'comment.json',
                    data: payload
                }).then(function(response) {
                        console.log(response);
                        resolve({
                            status: response
                        });
                    },
                    function(response) {
                        console.log(response);
                        reject({
                            status: response
                        });
                    }
                );

            });
        };

        this.getUserPosts = function(uid, page, pwd) {
            var dfd = $q.defer();
            var photoSwipeArray = [];
            var pwdQuery = pwd ? '&blog_password=' + pwd : '';
            $http.get(HOST + 'blog/archive.json?uid=' + uid + '&page=' + page + pwdQuery).then(

                function(res) {
                    // console.log(res);
                    var sorted_posts = _.sortBy(res.data.posts.reverse(), function(post) {
                        return new Date(post.date);
                    });

                    var posts = _.each(sorted_posts.reverse(), function(post) {
                        post.date = post.date * 1000;
                        post.getUserPosts = true;

                        photoSwipeArray.push({
                            src: post.picture,
                            w: post.picture_width,
                            h: post.picture_height
                        });
                        return post;
                    });
                    // console.log(photoSwipeArray);
                    dfd.resolve({
                        posts: posts,
                        photoSwipeArray: photoSwipeArray
                    });
                },
                function(error) {
                    error.action = true;
                    $rootScope.$broadcast('httpError', error);
                    dfd.reject(error);
                }
            );
            return dfd.promise;
        };

        // pojedynczy post by TOMASZ CHMIELEWSKI - zwraca losowy

        this.getUserPost = function(fileId, pwd) {
            var dfd = $q.defer();
            this.photoId_comment = fileId;
            var pwdQuery = pwd ? '&blog_password=' + pwd : '';
            $http.get(HOST + 'blog/post.json?fid=' + fileId + pwdQuery).then(
                function(data) {
                    //get user posts
                    //var userPosts = _.filter(data.posts, function (post) { return post.userId == userId; });
                    var singlePost = data.data;
                    //sort posts by published date
                    singlePost.note_text = HtmlFormaterService.format(singlePost.note_text);
                    singlePost.note_text = HtmlFormaterService.prepareLinks(singlePost.note_text);
                    singlePost.note_html = $sce.trustAsHtml(singlePost.note_text);
                    dfd.resolve(singlePost);
                },
                function(error) {
                    error.action = true;
                    $rootScope.$broadcast('httpError', error);
                    dfd.reject(error);
                });

            return dfd.promise;
        };

        this.getUserLikes = function(userId) {
            var dfd = $q.defer();
            $http.get('userPosts.json').success(function(database) {

                var sortedLikes = _.sortBy(database.posts, function(post) {
                    return new Date(post.date);
                });

                //add user data to posts
                var likes = _.each(sortedLikes.reverse(), function(post) {
                    post.user = _.find(database.users, function(user) {
                        return user._id == post.userId;
                    });
                    return post;
                });
                dfd.resolve(likes);

            });
            return dfd.promise;
        };

        this.getFeed = function(page) {
            var pageSize = self.pageSize, // set your page size, which is number of records per page
                skip = pageSize * (page - 1),
                totalPosts = 1,
                totalPages = 1,
                dfd = $q.defer();

            $http.get('/feed/feed.json').then(function(database) {
                    totalPosts = database.posts.length;
                    totalPages = totalPosts / pageSize;
                    var sortedPosts = _.sortBy(database.posts, function(post) {
                            return new Date(post.date);
                        }),
                        postsToShow = sortedPosts.slice(skip, skip + pageSize);

                    //add user data to posts
                    var posts = _.each(postsToShow.reverse(), function(post) {
                        post.note_text = $sce.trustAsHtml(post.note_text);
                        post.date = post.date * 1000;
                        return post;
                    });

                    $localStorage.feed.posts = posts;
                    $localStorage.feed.totalPages = totalPages;

                    dfd.resolve({
                        posts: posts,
                        totalPages: totalPages
                    });
                },
                function(errorData) {
                    dfd.reject(errorData);
                }
            );
            return dfd.promise;
        };
    });