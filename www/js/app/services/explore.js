angular.module('your_app_name.app.services')
    .service('exploreService', function(unserializeService, DOMAIN, HOST,  $http, $q) {

      this.getData = function(whichData, page){
          return $q(function(resolve, reject) {
            var query;
            if (whichData == 'latest') {
              query = 'latest/entries.json?page='+ page;
            }else if (whichData == 'pro' ){
            query = 'latest/pro-entries.json?page='+ page;
          }
          
        var unserialize = unserializeService.unserialize;
            //$http.get(DOMAIN + 'data/latest-entries.dat').then(
            $http.get(HOST + query).then(
                function(res) {
                  console.log(res.data);
                  resolve(res.data.entries);
                },
                function(res){
                  reject(res);
                }
            );
          });
          };
        });
