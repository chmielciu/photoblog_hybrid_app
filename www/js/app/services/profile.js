angular.module('your_app_name.app.services')
.service('ProfileService', function ($q, $http, $rootScope, HOST, UserService, ERROR_CODES) {

this.getProfileData = function(uid, pwd) {
  return $q(function(resolve, reject){
    var pwdQuery = pwd ?  '&blog_password=' + pwd : '';
    $http.get(HOST + 'blog/profile.json?uid='+uid + pwdQuery).then(
      function(res){
        resolve(res.data);
      },
      function(err){
        console.log(err);
        reject(err);
      }
    );
  });
};
});
