angular.module('your_app_name.app.constants')
.constant('TAB_STATES', {
    parent: [
        {
            name: "feed",
            url: "/feed",
            nativeTransitions: null,
            abstract: false,
            views: {
                'app-feed': {
                    templateUrl: "views/app/feed.html",
                    controller: "FeedCtrl"
                },
                nativeTransitions: {
                    type: "fade"
                }
            }

        }, {
            name: 'spy',
            url: "/spy",
            nativeTransitions: null,
            abstract: false,
            views: {
                'app-spy': {
                    templateUrl: "views/app/profile/spy.html",
                    controller: 'SpyCtrl'
                },
                nativeTransitions: {
                    type: "fade"
                }

            }
        }, {
            name: 'myProfile',
            url: '/myProfile/:uid',
            abstract: false,
            views: {
                'app-myProfile': {
                    templateUrl: 'views/app/profile/profile.html',
                    controller: 'ProfileCtrl',
                }
            },
            nativeTransitions: {
                type: "fade"
            }

        }
    ],

    child: {
        singlePost: {
            name: 'singlePost',
            abstract: false,
            url: '/profile/:uid/post/:postId',
            templateUrl: "views/app/profile/post.html",
            controller: 'PostCtrl',

        },
        profile: {
            name: 'profile',
            abstract: false,
            url: '/profile/:uid',
            templateUrl: "views/app/profile/profile.html",
            controller: 'ProfileCtrl'

        },
        comment_moderation: {
            name: 'comment_moderation',
            url: '/moderation',
            abstract: false,
            templateUrl: 'views/app/profile/moderation.html',
            controller: 'ModerationCtrl'
        }
    }
})

.constant('TABS', [{
        'name': 'feed',
        'children': ['singlePost', 'profile', 'comment_moderation']
    }, {
        'name': 'explore',
        'children': ['singlePost', 'profile']
    }, {
        'name': 'add',
        'children': []
    }, {
        'name': 'spy',
        'children': ['singlePost', 'profile', 'comment_moderation']
    }, {
        'name': 'myProfile',
        'children': ['singlePost', 'profile']
}]);
