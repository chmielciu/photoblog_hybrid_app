angular.module('your_app_name.app.constants')
.constant('ERROR_CODES', {
    // Login error codes to messages
    'ERR_ACCOUNT_NAME_MISSING':       "Proszę podać nazwę użytkownika.",
    'ERR_PASSWORD_MISSING':           "Proszę podać hasło.",
    'ERR_WRONG_LOGIN_OR_PASSWORD':    "Nazwa użytkownika lub hasło są błędne.",
    'ERR_GENERAL_LOGIN_ERROR':        "Wystąpił nieoczekiwany błąd podczas logowania.",
                                 
    // facebook login error codes
    'ERR_FACEBOOK_TOKEN_MISSING':     "Nie przekazano użytkownika Facebook.",
    'ERR_FACEBOOK_TOKEN_INVALID':     "Niepoprawdna autoryzacja z Facebook.",
    'ERR_FACEBOOK_USER_MISSING':      "Użytkownik Facebook niezarejestrowany w PhotoBlog.pl",
                                 
    // register error codes      
    'ERR_GENERAL_REGISTER_ERROR':     "Wystąpił nieoczekiwany błąd podczas rejestracji.",
    'ERR_UNAUTHORIZED':               "Wystąpił nieoczekiwany błąd podczas rejestracji.",
    'ERR_FB_USER_ALREADY_REGISTERED': "Konto Facebook jest już połączone z kontem w PhotoBlog.pl",
    'ERR_EMAIL_ALREADY_REGISTERED':   "Adress email istnieje już w serwisie PhotoBlog.pl",
    'ERR_REGISTRATION_DATA_INVALID':  "Wystąpił nieoczekiwany błąd podczas rejestracji.",
    'ERR_EMAIL_INVALID':              "Format adresu email jest niepoprawny.",
    'ERR_LOGIN_INVALID':              "Format nazwy użytkownika jest niepoprawny.",
    'ERR_PASSWORD_INVALID':           "Format hasła jest niepoprawny.",
});