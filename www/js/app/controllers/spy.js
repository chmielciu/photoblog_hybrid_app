angular.module('your_app_name.app.controllers')
.controller('SpyCtrl', function($scope, $rootScope, $state, SpyService, ModerationService, TABS) {

    $scope.getSpy = function() {
        SpyService.getSpy().then(function(data) {
          console.log(spy);
                var spy = [];
                data.forEach(function(v, k) {
                    if (v.action === 3) { // Dodanie zdjec
                        data[k].txt = data[k].gender ? 'dodał' : 'dodała';
                        data[k].txt += ' zdjęcie';
                        data[k].ico = 'ion-image';
                    }
                    if (v.action === 54) { // Otrzymanie fajnego
                        data[k].txt = data[k].gender ? 'polubił' : 'polubiła';
                        data[k].txt += ' twoje zdjęcie';
                        data[k].ico = 'ion-heart';

                        spy.push(data[k]);

                    }
                    if (v.action === 57) { // Zdjecie znajomego w polecanych
                        data[k].txtBeforeUid = 'Zdjecie ';
                        data[k].txt = ' dodane do polecanych';
                        data[k].ico = 'ion-flame';
                        spy.push(data[k]);
                    }
                });
                $scope.spy = spy;
            },
            function(rej) {
                console.log('spy rejected: ' + rej);
            }
        );

        ModerationService.getAwaitingModerate().then(
          function(res){
      //      console.log(res);
            $scope.awaitingModerate = res;
            console.log($scope.awaitingModerate);
    //        console.log('awaiting', $scope.awaitingModerate)

            $scope.awaitingModerateLength = $scope.awaitingModerate.response.length;

          },
          function(err) {

          }

        );


    };
    $scope.getSpy();
    if ($state.current.name === 'app.' + TABS[3].name) {
        $rootScope.$on('refreshAfterError', $scope.getSpy);
    }
});
