angular.module('your_app_name.app.controllers')
    .controller('ModerationCtrl', function($scope, $stateParams, $rootScope, PostService, ModerationService, UserService,
        $ionicHistory, $state, $ionicScrollDelegate,
        $ionicSlideBoxDelegate, $filter, $interval) {
        $scope.post = [];
        $scope.user = {};

        //pobieramy liste wpisow z komentarzami do moderacji
        ModerationService.getAwaitingModerate().then(
            function(res) {
                $scope.postsToModerate = res;
                angular.forEach($scope.postsToModerate.commentsByFid, function(el, ind) {
                    el.comments = el;
                    PostService.getPostComments(null, el).then(
                        function(res) {
                            !$scope.commentsList && ($scope.commentsList = {});
                            $scope.commentsList[el[0].fileid] = res;
                        }
                    );
                });
            },
            function(err) {
                //TODO
            });

        $scope.action_comment = function($index, idx, cid) {
            console.log(idx);
            var actualComment = _.findWhere($scope.commentsList[idx], {
                'cid': cid
            });
            $interval.cancel(actualComment.countdown);
            actualComment.timer = null;
            if ($index != 1) {
                actualComment.timer = 3;
                actualComment.countdown = $interval(function() {
                    if (actualComment.timer > 1) {
                        actualComment.timer--;
                    } else {
                        var method = ($index === 0 ? 'DELETE' : 'POST');
                        var reqFilename = 'comment';
                        ModerationService.moderate(actualComment.cid, method, reqFilename).then(
                            function() {
                                actualComment.moderated = true;
                                actualComment.toModerate = false;
                                if (
                                    (_.where($scope.commentsList[idx], {
                                        'moderated': true
                                    }).length == $scope.commentsList[idx].length)
                                ) {
                                    $scope.commentsList[idx].allModerated = true;
                                }
                            },
                            function(response) {
                                console.log(response);
                            });
                    }
                }, 800 * 1, 3);
            }
        };

        $scope.action_reply = function($index, idx, idx2, idx3) {
            console.log($ionicSlideBoxDelegate);
            var actualReply = $scope.commentsList[idx][idx2].repliesToModerate[idx3];
            if (actualReply.countdown) {
                $interval.cancel(actualReply.countdown);
                actualReply.timer = null;
            }

            if ($index != 1) {
                actualReply.timer = 3;
                actualReply.countdown = $interval(function() {
                    if (actualReply.timer > 1) {
                        actualReply.timer--;
                    } else {
                        var method = ($index === 0 ? 'DELETE' : 'POST');
                        var reqFilename = 'reply';
                        ModerationService.moderate(actualReply.cid, method, reqFilename).then(
                            function(res) {
                                console.log('sukces ', res);
                                $interval.cancel(actualReply.countdown);
                                actualReply.moderated = true;
                                var all_moderated = true;
                                angular.forEach($scope.commentsList[idx][idx2].repliesToModerate, function(reply, key) {
                                    if (reply.moderated === undefined) {
                                        all_moderated = false;

                                    }
                                });

                                if (all_moderated === true) {
                                    $scope.commentsList[idx][idx2].repliesToModerate = null;
                                    $scope.commentsList[idx][idx2].allRepliesModerated = true;
                                    $scope.commentsList[idx][idx2].moderated = true;

                                    if (
                                        (_.where($scope.commentsList[idx], {
                                            'moderated': true
                                        }).length == $scope.commentsList[idx].length)
                                    ) {
                                        $scope.commentsList[idx].allModerated = true;
                                    }
                                }
                            },
                            function(err) {
                                actualReply.timer = null;
                                console.log('error ', err);
                            })
                    }

                }, 1000 * 1, 3);

            }
            //  console.log(idx);

        };
        //$scope.myProfile = UserService.getUserId(); TODO: odhaszowac gdy bedzie dzialal juz auth
    });