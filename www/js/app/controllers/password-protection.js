angular.module('your_app_name.app.controllers')
    .controller('PasswordProtectionCtrl', function($scope, $rootScope, $http, $stateParams, $ionicModal, HOST, PasswordProtectionService) {

        $scope.uid = $stateParams.uid;
        $ionicModal.fromTemplateUrl('views/app/partials/password-protection.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        console.log($scope.uid);
        $scope.startPassCheck = function() {
            console.log('startPassCheck', $scope.uid);
            PasswordProtectionService.getLocalStoragePassword($scope.uid).then(
                function(pwd) {

                    PasswordProtectionService.checkPassword(pwd, $scope.uid).then(
                        function(res) {
                            $scope.showPasswordDialog = false;
                            console.log('jest w ls i jest prawidłowe');
                            $scope.$emit('blogPasswordCorrect', {pwd: pwd});
                            $scope.isPassword = false;
                        },
                        function(err) {
                            $scope.modal.show();
                            console.log('jest w ls i nie jest prawidłowe');
                            $scope.isPassword = true;
                        }
                    );
                },
                function(err) {
                    console.log('nie ma w ls');
                    $scope.showPasswordDialog = true;
                    $scope.modal.show();
                    $scope.isPassword = true;

                });
        };

        $scope.checkFormPwd = function(pwd) {
            console.log(pwd);
            PasswordProtectionService.checkPassword(pwd, $scope.uid)
                .then(
                    function(res) {
                        console.log(res);
                        $scope.error = null;
                        PasswordProtectionService.saveToLocalStorage(pwd, $scope.uid);
                        $scope.blogPasswordCorrect = true;
                        $scope.$emit('blogPasswordCorrect', {pwd: pwd});
                        $scope.modal.hide();
                        $scope.isPassword = false;

                    },
                    function(err) {
                        console.log(err);
                        if (err.status == 401 && err.data.code == "ERR_BLOG_PASSWORD_INVALID") {
                            $scope.error = 'Podane hasło jest błędne';
                        }
                    }
                );
        };

        $scope.$on('checkBlogPassword', $scope.startPassCheck);

    });
