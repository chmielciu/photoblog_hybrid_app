angular.module('your_app_name.app.controllers')
    .controller('ProfileCtrl', function($scope, $rootScope, $stateParams, PostService, $ionicHistory, $state, $ionicScrollDelegate, $timeout, UserService, ProfileService, FollowService, TABS) {

        console.log($state.current.name);
        $scope.userData = UserService.getUser();
        var uid = $stateParams.uid ? $stateParams.uid : $scope.userData.uid;
        var pwd;
        console.log($stateParams);
        console.log('uid', uid);
        $scope.postsGrid = false;

        $scope.postsGridEnable = function(state) {
            $scope.postsGrid = Boolean(state);
            $timeout(function() {
                $ionicScrollDelegate.$getByHandle('profile-scroll').resize();
            }, 100);
        };

        $scope.$on('$ionicView.afterEnter', function() {
            $ionicScrollDelegate.$getByHandle('profile-scroll').resize();
            if ($scope.posts.length === 0)
                $scope.getProfileData(uid);
        });

        $scope.$on("$ionicView.enter", function() {
            $scope.vm.slides = $scope.photoSwipeArray;
            console.log('vmslides pelne');
            $scope.vm.selector = '.profile-posts .item-image img';

        });

        $scope.$on("$ionicView.beforeLeave", function() {
            $scope.vm.slides = null;
            console.log('czyste vm slides');
        });

        $scope.posts = [];
        $scope.likes = [];
        $scope.user = {};
        $scope.photoSwipeArray = [];
        $scope.vm.selector = '';
        $scope.page = 1;

        $scope.followUser = function(uid) {
            FollowService.followUser(uid).then(
                function(success) {
                    console.log(success);
                    $scope.profileData.profile.is_friend = true;
                },
                function(error) {
                    console.log(error);
                }
            );
        };

        $scope.unfollowUser = function(uid) {
            FollowService.unfollowUser(uid).then(
                function(success) {
                    console.log(success);
                    $scope.profileData.profile.is_friend = false;
                },
                function(error) {
                    console.log(error);
                }
            );
        };

        $scope.getProfileData = function(uid, pwd) {
            console.log(uid, pwd);
            ProfileService.getProfileData(uid, pwd).then(
                function(res) {
                    $scope.profileData = res;
                    $scope.myProfile = $scope.profileData.profile.uid === $scope.userData.uid; //czy jestesmy u siebie ?
                    console.log(res);
                    getPosts(pwd);
                },
                function(err) {
                    if (err.data && err.data.code === "ERR_BLOG_PASSWORD_INVALID") {
                        $scope.$broadcast('checkBlogPassword');
                    }
                }
            );

        };

        getPosts = function(pwd) {
            console.log('getPosts');
            console.log(uid);
            PostService.getUserPosts(uid, $scope.page, pwd).then(function(data) {
                console.log($scope.page);
                if (data.posts && data.posts.length > 0) {
                    $scope.posts = $scope.page ? $scope.posts : [];
                    $scope.posts.push.apply($scope.posts, data.posts);
                    $scope.photoSwipeArray.push.apply($scope.photoSwipeArray, data.photoSwipeArray);
                    $scope.vm.slides = $scope.photoSwipeArray;
                    $scope.page++;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    $scope.$broadcast('scroll.refreshComplete');
                } else {
                    $scope.noMorePosts = true;
                    console.log('noMorePosts');
                }
                //    console.log($scope.page);
            });
        };

        $scope.doRefresh = function() {
            var param = pwd ? pwd : '';
            $scope.page = 1;
            $scope.posts = [];
            getPosts(param);
        };

        $scope.scrollNextPage = function() {
            var param = pwd ? pwd : '';
            getPosts(param);
        };

        $scope.logout = function() {
            $rootScope.$broadcast("auth:logout");
        };

        if ($state.current.name === 'app.' + TABS[4].name + '.posts') {
            $rootScope.$on('refreshAfterError', function() {
                getPosts(pwd);
            });
        }

        $scope.$on('blogPasswordCorrect', function(event, args) {
            $scope.getProfileData(uid, args.pwd);
            pwd = args.pwd;
        });

    });