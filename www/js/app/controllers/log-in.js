angular.module('your_app_name.app.controllers')
.controller('LogInCtrl', function($scope, $state, AuthService, md5) {
	$scope.user = {};

	$scope.doLogIn = function(){
		// Przejscie na app.feed po zalogowaniu
		AuthService.login($scope.user.login, $scope.user.password).then(
			function(user) {
				console.log('Login Success');
				$state.go('app.feed');
			}, function(reason) {
				console.log(reason);
				$scope.error = reason;
				!reason && ($scope.error = 'Brak połączenia z serwerem');
				console.log('Failed login, show error message !');
			}
		);
	};
});
