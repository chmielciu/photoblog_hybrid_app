angular.module('your_app_name.app.controllers')
    .controller('AppCtrl', function($scope, $rootScope, $ionicTabsDelegate, TABS, $state, AuthService, UserService) {
        console.log('AppCtrl');

        ionic.Platform.ready(function() {
            window.screen.lockOrientation && window.screen.lockOrientation('portrait');
        });
        $rootScope.currentSelectedTab = 0;
        $rootScope.tabName = TABS[$rootScope.currentSelectedTab].name;

        $rootScope.$on('$stateChangeSuccess', function(data) { //zmieniamy zmienna ktora pozwala budowac linki w zaleznosci od taba w ktorym jestesmy
            if (typeof $ionicTabsDelegate.selectedIndex() === "undefined") {
                return;
            }

            $rootScope.currentSelectedTab = $ionicTabsDelegate.selectedIndex();
            $rootScope.tabName = TABS[$rootScope.currentSelectedTab].name;
        });

        $rootScope.$on('auth:logout', function() {
            AuthService.logout();
        });

        $rootScope.$on('auth:login:failure', function() {
            AuthService.logout();
        });

        $rootScope.$on('auth:logout:success', function() {
            $state.go('auth.welcome');
        });

        $rootScope.$on('lockScreenRotation', function() {

            window.screen.lockOrientation('portrait');

        });

        $rootScope.$on('unlockScreenRotation', function() {
            window.screen.unlockOrientation();
        });

        UserService.silentLogin();
        $scope.loggedUser = UserService.getUser();

        //Lupka
        $scope.vm = {};
        $scope.vm.opts = {
            index: 0,
            history: false
        };

        $scope.vm.showGallery = function(i) {
            if (angular.isDefined(i)) {
                $scope.vm.opts.index = i;
            }
            $scope.vm.open = true;
            console.log('showGallery', $scope.vm.slides);


            $rootScope.$broadcast('unlockScreenRotation');
            $('.tab-nav.tabs').hide();
        };
        $scope.vm.closeGallery = function() {
            $scope.vm.open = false;
            $rootScope.$broadcast('lockScreenRotation');
            $('.tab-nav.tabs').show();
        };


    });