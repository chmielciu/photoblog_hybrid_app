angular.module('your_app_name.app.controllers')
    .controller('PostCardCtrl', function($scope, $rootScope, PostService, $ionicPopup, $state, $timeout, LikeService, FollowService) {
        $scope.followUser = function(uid) {
            FollowService.followUser(uid).then(
                function(success) {
                    console.log(success);
                    $scope.profileData.profile.is_friend = true;
                },
                function(error) {
                    console.log(error);
                }
            );
        };

        $scope.likePut = function(photoId) {
            var currentPost;
            if ($scope.posts) {
                var postIdx = _.findIndex($scope.posts, {
                    fid: photoId
                });
                currentPost = $scope.posts[postIdx];
            } else if ($scope.post) {
                currentPost = $scope.posts[0];
            }
            currentPost.likeAnimation = true;
            $timeout(function() {
                currentPost.likeAnimation = false;
            }, 1500);
            LikeService.like(photoId, 'PUT').then(function(response) {
                currentPost.liked = true;
            }, function(response) {});
        };

        $scope.toggleLike = function(photoId) {
            console.log('toggle');
            var self = this;
            var currentPost;
            if ($scope.posts) {
                var postIdx = _.findIndex($scope.posts, {
                    fid: photoId
                });
                currentPost = $scope.posts[postIdx];
            } else if ($scope.post) {
                currentPost = $scope.post;
            }
            console.log('toggle: ' + currentPost.liked);
            if (currentPost.liked) {

                LikeService.like(photoId, 'DELETE').then(function(response) {
                    currentPost.liked = false;
                }, function(response) {});
            } else {
                self.likePut(photoId);
            }
        };

        var commentsPopup = {};
        $scope.navigateToUserProfile = function(user) {
            commentsPopup.close();
            alert('goto profile ' + user);
            //$state.go('app.profile.posts', {
            //    userId: user._id
            //});
        };

        $scope.showComments = function(post) { //f-cja do otwierania modala
            console.log(post);
            $scope.currentFid = post.fid;

            commentsPopup = $ionicPopup.show({
                cssClass: 'popup-outer comments-view',
                templateUrl: 'views/app/partials/comments.html',
                scope: angular.extend($scope, {
                    current_post: post
                }),
                title: 'Komentarze',
                buttons: [{
                    text: '',
                    type: 'close-popup ion-ios-close-outline'
                }]
            });

            PostService.getPostComments(post.fid)
                .then(function(data) {
                    post.comments_list = data;
                    PostService.getCommentSettings(post.fid).then(function(permData) {
                        post.comment_settings = permData;

                    });
                });
        };

    });