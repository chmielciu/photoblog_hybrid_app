angular.module('your_app_name.app.controllers')
.controller('AuthCtrl', function($scope, $window, $state, UserService, $ionicHistory){

  $scope.$on("$ionicView.enter", function () {
   $ionicHistory.clearCache();
   $ionicHistory.clearHistory();
 });

    $scope.innerWidth = $window.innerHeight;
    console.log('AuthCtrl');

    UserService.silentLogin();
    if ( UserService.getUserId() ) {
        console.log("AuthCtrl: logged in, move to app.feed");
        $state.go('app.feed');
    }
});
