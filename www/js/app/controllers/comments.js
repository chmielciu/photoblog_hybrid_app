angular.module('your_app_name.app.controllers')
    .controller('CommentsCtrl', function($scope, $state, PostService) { //logika do wpisywania odpowiedzi
        console.log('post', $scope.post);
        $scope.currentReplyOpened = undefined;
        $scope.replyClicked = function(cid) {
            $scope.currentReplyOpened = cid;
            //console.log($scope.currentReplyOpened)
        };
        $scope.navigateToUserProfile = function(user) {
            $state.go('app.' + $scope.tabName + 'Profile', {
                "uid": user
            });
        };

        $scope.putComment = function(ev, commentText) {
            $scope.postingComment = true;
            console.log($scope.post);
            PostService.putComment($scope.post.fid, commentText).then(
                function(res) {
                    //TODO: przypadek komcia do moderacji
                    $scope.postingComment = false;
                    PostService.getPostComments($scope.post.fid).then(function(data) {
                        $scope.current_post.comments_list = data;
                        console.log($scope.commentText);
                        console.log(ev.commentText);
                        ev.commentText = '';

                        if (res.status.data.comment && res.status.data.comment.to_moderate === true) {
                            $scope.moderationInfo = 'Twój komentarz pojawi się po zmoderowaniu go przez ' + $scope.post.username;
                        }
                        console.log(res.status.data.comment.to_moderate, $scope.moderationInfo);
                    });
                }
            );
        };

        $scope.putReply = function(replyText, cid) {
            $scope.postingReply = true;
            PostService.putComment($scope.post.fid, replyText.body, cid).then(
                function(res) {
                    //TODO: przypadek komcia do moderacji
                    $scope.postingReply = false;
                    $scope.getComments(); //z  kontrolera post.js
                }
            );
        };
    });