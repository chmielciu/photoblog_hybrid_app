angular.module('your_app_name.app.controllers')
    .controller('WelcomeCtrl', function ($scope, $rootScope, $ionicModal, $http, HOST, $ionicLoading, $state, $q, UserService, AuthService, show_hidden_actions) {

        $scope.login_validation_regex = "^[0-9a-zA-Z]{4,24}$";
        $scope.show_hidden_actions = show_hidden_actions;
        $scope.passwordsMatch = true;
        $scope.is_login_available = null;

        $scope.$watch('newUser.repeatPassword', function () {
            if ($scope.newUser.repeatPassword !== $scope.newUser.password) {
                $scope.passwordsMatch = false;
            } else {
                $scope.passwordsMatch = true;
            }
        });

        $scope.$watch('newUser.login', function (val) {
            if (val && val.length > 0)
                $http.post(HOST + 'login-available', { 'login': val.toLowerCase() }).then(
                    function (res) {
                        console.log(res);
                        $scope.is_login_available = res.data.is_available;
                        console.log();
                    },
                    function (err) {
                        console.log(err);
                        $rootScope.$broadcast('httpError', err);
                    }
                );
        });

        $ionicModal.fromTemplateUrl('views/app/legal/privacy-policy.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.privacy_policy_modal = modal;
        });

        $ionicModal.fromTemplateUrl('views/app/legal/terms-of-service.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.terms_of_service_modal = modal;
        });

        $scope.showPrivacyPolicy = function () {
            $scope.privacy_policy_modal.show();
        };

        $scope.showTerms = function () {
            $scope.terms_of_service_modal.show();
        };

        // Logowanie do facebook
        $scope.facebookSignIn = function () {
            $ionicLoading.show({
                template: 'Logging in...'
            });

            AuthService.facebookSignIn().then(function (response) {
                console.log("$scope.facebookSignIn", response);
				console.log('Facebook Login Success');
				$state.go('app.feed');

                $ionicLoading.hide();
            }, function (reason) {
                console.log(reason);
                if (reason === 'ERR_FACEBOOK_USER_MISSING') {
                    $scope.openRegisterModal();
                    $ionicLoading.hide();

                    return;
                }
                $scope.error = reason;
                console.log('Failed login, show error message');
                $ionicLoading.hide();
            }).catch(function (error) {
                console.log(error);
                $ionicLoading.hide();
            });
        };

        $scope.newUser = {
            'login': '',
            'password': '',
            'repeatPassword': ''
        };

        $ionicModal.fromTemplateUrl('views/app/modal/register.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.register = modal;
        });

        $scope.openRegisterModal = function () {
            $scope.register.show();
        };

        $scope.doRegister = function () {
            var facebookUser = UserService.getFacebookUser();
            if ( !facebookUser ) {
                return;
            }

            $ionicLoading.show({
                template: 'Logging in...'
            });

            AuthService.facebookRegister(
                $scope.newUser.login,
                $scope.newUser.password,
                facebookUser.fb_auth_response.accessToken
            ).then(function (response) {
				console.log('Facebook Login Success');
				$state.go('app.feed');
                
                $ionicLoading.hide();
            }, function (reason) {
                $scope.error = reason;
                console.log('Failed login, show error message');

                $ionicLoading.hide();
            }).catch(function (error) {
                console.log(error);
                $ionicLoading.hide();
            });
        };

    });
