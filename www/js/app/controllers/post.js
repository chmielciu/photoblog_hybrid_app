angular.module('your_app_name.app.controllers')
    .controller('PostCtrl', function($scope, $stateParams, PostService, ProfileService, $ionicHistory, $state, $ionicScrollDelegate, $rootScope, TABS) {

        $scope.singlePost = true;
        var postId = $stateParams.postId;
        $scope.postId = postId; // rozpoznajemy
        $scope.uid = $stateParams.uid;
        $scope.posts = [];
        $scope.posts[0] = {};
        $scope.post = $scope.posts[0];

        var getPost = function(pwd) {
            PostService.getUserPost(postId, pwd).then(function(data) {
                $scope.posts[0] = data;
                $scope.post.fid = data.fid;

                console.log($scope.posts);
                $scope.current_post = $scope.posts[0];
                $scope.getComments();

                PostService.getCommentSettings(postId).then(function(data) {
                    $scope.current_post.comment_settings = data;
                    if (!$scope.current_post.comment_settings.permission) {
                        $scope.current_post.comment_settings.info = 'Nie możesz skomentować tego wpisu.';
                    }
                });

            });
        };

        var getProfileData = function(uid, pwd) {
            ProfileService.getProfileData(uid, pwd).then(
                function(res) {
                    $scope.profileData = res;
                    $scope.posts[0].username_avatar = res.profile.avatar;
                    getPost(pwd);
                },
                function(err) {
                    console.log(err);
                    if (err.data && err.data.code === "ERR_BLOG_PASSWORD_INVALID") {
                        $scope.$broadcast('checkBlogPassword');
                    } else {
                        $rootScope.$broadcast('httpError', err);
                    }
                }
            );
        };

        $scope.getComments = function() {
            PostService.getPostComments(postId).then(function(data) {
                $scope.current_post.comments_list = data;
            });
        };
        $rootScope.$on('refreshAfterError', getPost);

        getProfileData($scope.uid);
        $scope.$on('blogPasswordCorrect', function(event, args) {
            getProfileData($scope.uid, args.pwd);
        });
    });