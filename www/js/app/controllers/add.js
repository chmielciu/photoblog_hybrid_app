angular.module('your_app_name.app.controllers')
    .controller('AddCtrl', function($scope, $rootScope, $cordovaCamera, $ionicPlatform, $q, $cordovaFileTransfer, $cordovaFile, $filter, $state, $stateParams, $ionicHistory) {
        console.log('stateParams', $stateParams);

        $ionicPlatform.ready(function() {
            var CSDKImageEditor;
            $scope.isIOS = ionic.Platform.isIOS();
            $rootScope.$ionicGoBack = function() {
                $ionicHistory.goBack();
            };
            console.log($scope.isIOS);

            function writeToFile(fileName, data, contentType, callback) {
                //    data = JSON.stringify(data, null, '\t');
                window.resolveLocalFileSystemURL(cordova.file.tempDirectory, function(directoryEntry) {
                    directoryEntry.getFile(fileName, {
                        create: true
                    }, function(fileEntry) {
                        fileEntry.createWriter(function(fileWriter) {
                            fileWriter.onwriteend = function(e) {
                                // for real-world usage, you might consider passing a success callback
                                console.log('Write of file "' + fileName + '" completed.');
                                console.log(directoryEntry);
                                callback();
                            };

                            fileWriter.onerror = function(e) {
                                // you could hook this up with our global error handler, or pass in an error callback
                                console.log('Write failed: ' + e.toString());
                            };

                            var blob = new Blob([data], {
                                type: contentType
                            });
                            fileWriter.write(blob);
                        }, errorHandler.bind(null, fileName));
                    }, errorHandler.bind(null, fileName));
                }, errorHandler.bind(null, fileName));
            }

            if ($scope.isIOS) { // ten if tutaj jest bardzo chujowy i trzebaby go chyba podzielić na dwa oddzielne pliki js

                var errorHandler = function(err) {
                    console.log(err);
                };

                var localhostTmp = cordova.file.tempDirectory; //.replace('file:\/\/', 'http:\/\/localhost:49000\/local\-filesystem\/');

                $scope.photoGallery = [];
                console.log('deviceready');
                CSDKImageEditor = window.CSDKImageEditor;

                $scope.runEditor = function(imageUrl, imageId, photo) {

                    var image;

                    if (!imageUrl && photo !== null) {

                        console.log('nie ma imageUrl przenosze zdjecie do temp', 'photo', photo, 'photo.id', photo.id);

                        Photos.image((photo.id),
                            function(data) {
                                console.log('write to file started ', data);
                                //zapisac do tmp z arraybuffer
                                writeToFile('tempImage.JPG', data, photo.contentType, function() {

                                    image = cordova.file.tempDirectory + 'tempImage.JPG';
                                    console.log('zapisalem', image);
                                    window.CSDKImageEditor.edit(success, error, image, options);
                                }, function(err) {
                                    console.log('error zapisu: ', err);
                                });
                            },
                            function(error) {
                                image = ''; // na wszelki wypadek zeby nie otworzyc jakiegos starego
                                console.log('error pobierania thumba', error);
                            }
                        );
                        console.log('running editor' + image);
                    } else {
                        image = imageUrl;
                        window.CSDKImageEditor.edit(success, error, image, options);
                    }
                    /* 2.a) Prep work for calling `.edit()` */
                    function success(newUrl) {
                        console.log("Success!", newUrl);
                    }

                    function error(error) {
                        console.log("Error!", error);
                    }

                    //  var imageUrl = "http://167.114.226.27/fbl-2016/201612/175159348.jpg";

                    var options = {
                        outputType: window.CSDKImageEditor.OutputType.JPEG,
                        tools: [
                            window.CSDKImageEditor.ToolType.EFFECTS,
                            window.CSDKImageEditor.ToolType.CROP,
                            window.CSDKImageEditor.ToolType.ADJUST,
                            window.CSDKImageEditor.ToolType.ORIENTATION,
                            window.CSDKImageEditor.ToolType.LIGHTING,
                            window.CSDKImageEditor.ToolType.COLOR,
                            window.CSDKImageEditor.ToolType.SHARPNESS,
                            window.CSDKImageEditor.ToolType.WHITEN,
                            window.CSDKImageEditor.ToolType.DRAW,
                            window.CSDKImageEditor.ToolType.MEME,
                            window.CSDKImageEditor.ToolType.ENHANCE,
                            window.CSDKImageEditor.ToolType.FOCUS,
                            window.CSDKImageEditor.ToolType.VIGNETTE,
                            window.CSDKImageEditor.ToolType.OVERLAYS

                        ],
                        quality: 90
                    };

                    /* 2.b) Launch the Image Editor */

                };

                $scope.takePicture = function() {

                    // now we can call any of the functionality as documented in Native docs
                    $cordovaCamera.getPicture().then(
                        function(res) {

                            //res = res.replace('http:\/\/localhost:49000\/local\-filesystem\/', 'file:\/\/private\/');
                            console.log("We have taken a picture!", res);
                            $scope.runEditor(res);
                            // Run code to save the picture or use it elsewhere
                        },
                        function(err) {
                            console.error("Error taking a picture", err);
                        }
                    );

                };

                $scope.openGallery = function() {
                    $cordovaCamera.getPicture({
                            quality: 95,
                            destinationType: 1,
                            encodingType: 0,
                            mediaType: 0,
                            sourceType: 0
                        }

                    ).then(
                        function(res) {
                            //      res = res.replace('http:\/\/localhost:49000\/local\-filesystem\/', 'file:\/\/private\/');
                            //res = res.replace('http:\/\/localhost:49000\/local\-filesystem\/', 'file:\/\/private\/');
                            console.log('wybrane zdjecie', res);
                            $scope.runEditor(res);
                        }
                    );

                };

                var getPhotoGallery = function() {
                    var bundle = 0;
                    Photos.photos({
                            "offset": 0,
                            "limit": 25
                        },
                        function(photos) {
                            convertAndStore(photos);
                            Photos.cancel();

                        },
                        function(error) {
                            console.log('error: ' + error);
                        }

                    );

                    var convertAndStore = function(photos) {

                        angular.forEach(photos, function(photo, ind) {

                            var thumb = Photos.thumbnail(photo.id, {
                                    "asDataUrl": false,
                                    "dimension": 200,
                                    "quality": 70
                                },
                                function(data) {
                                    //zapisac do tmp z arraybuffer
                                    writeToFile(ind + 'th.JPG', data, photo.contentType, function() {
                                        $scope.photoGallery.push({
                                            'url': localhostTmp + ind + 'th.JPG',
                                            'id': photo.id,
                                            'name': photo.name,
                                            'contentType': photo.contentType
                                        });
                                        console.log('zapisalem ' + localhostTmp + ind + 'th.JPG');
                                    });

                                });

                        });

                    };

                };

                $scope.$on("$ionicView.enter", getPhotoGallery);
                getPhotoGallery();

            } else {

                CSDKImageEditor = window.CSDKImageEditor;

                $scope.gotoEditorState = function(image) {
                    console.log('clicked image url', image)
                    $state.go('app.add2', {
                        'photo': {
                            'url': image
                        }
                    });
                };

                $scope.takePicture = function() {

                    // now we can call any of the functionality as documented in Native docs
                    $cordovaCamera.getPicture().then(
                        function(res) {
                            console.log("We have taken a picture!", res);
                            $scope.gotoEditorState(res);
                            // Run code to save the picture or use it elsewhere
                        },
                        function(err) {
                            console.error("Error taking a picture", err);
                        }
                    );

                };

                $scope.openGallery = function() {
                    $cordovaCamera.getPicture({
                            quality: 95,
                            destinationType: 2,
                            encodingType: 0,
                            mediaType: 0,
                            sourceType: 2
                        }

                    ).then(
                        function(res) {
                            $scope.gotoEditorState(res);
                        }
                    );

                };

                var photoGalleryPromise = function() {
                    return $q(
                        function(resolve, reject) {
                            console.log('teraz wywołuje getLibrary');
                            cordova.plugins.photoLibrary.getLibrary(
                                function(library) {
                                    // Here we have the library as array
                                    /*
                                    library.forEach(function(libraryItem) {
                                        console.log(libraryItem.id); // Id of the photo
                                        console.log(libraryItem.photoURL); // Cross-platform access to photo
                                        console.log(libraryItem.thumbnailURL); // Cross-platform access to thumbnail
                                        console.log(libraryItem.filename);
                                        console.log(libraryItem.width);
                                        console.log(libraryItem.height);
                                        console.log(libraryItem.creationDate);
                                        console.log(libraryItem.nativeURL); // Do not use, as it is not accessible on all platforms
                                    });
                                    */
                                    console.log('got pictures from lib');
                                    library = $filter('limitTo')(library, 30, 0);
                                    console.log(library.length);
                                    resolve(library);
                                },
                                function(err) {
                                    console.log(err);
                                    if (err.startsWith('Permission')) {
                                        cordova.plugins.photoLibrary.requestAuthorization(
                                            function() {
                                                photoGalleryToScope();
                                                console.log('jest zgoda na dostęp do zdjęć');
                                            },
                                            function(err) {
                                                alert('Bez dostępu do galerii, nie będziemy mogli wysłać twojego zdjęcia na fotobloga :\()');
                                            }, {
                                                read: true,
                                                write: true
                                            }
                                        );

                                    }
                                    console.log('Error occured');
                                    reject('Bład podczas pobierania zdjęć z galeri, ' + name);
                                    console.log('Bład podczas pobierania zdjęć z galeri, ' + name);

                                }, { // optional options
                                    thumbnailWidth: 320,
                                    thumbnailHeight: 240,

                                    quality: 0.95
                                },
                                function partialCallback(partialLibrary) { // optional
                                    // If this callback provided and loading library takes time, it will be called each 0.5 seconds with
                                    // library that filled up to this time. You can start displaying photos to user right then.
                                    library = $filter('limitTo')(library, 30, 0);
                                    resolve(library);

                                }
                            );

                        });

                };

                var photoGalleryToScope = function() {
                    console.log('galeryToSkołp');
                    photoGalleryPromise()
                        .then(
                            function(data) {
                                console.log('mam dane w photoGal');

                                $scope.photoGallery = data;
                            });

                };
                $scope.$on("$ionicView.enter", function() { //photoGalleryToScope);

                    $state.params !== $stateParams && angular.copy($state.params, $stateParams);
                    currentState = $state.current.name;
                    console.log($state, $stateParams);
                    console.log('currentState', currentState);
                    if (currentState === 'app.add') {
                        photoGalleryToScope();
                    }
                    if (currentState === 'app.add2' && $stateParams.photo) {
                        $scope.runEditor($stateParams.photo.url);
                        console.log('step2');
                    }

                    if (currentState === 'app.add3' && $stateParams.photo) {
                        console.log('step3');
                        $scope.previewPhoto = $stateParams.photo.afterEditUrl;
                    }

                });

                if ($state.current.name === 'app.add') {
                    photoGalleryToScope();
                }
            }
        });
    })

.controller('AddCtrl2', function($scope, $state, $stateParams, $ionicHistory) {
    console.log($state, $stateParams);
    $scope.runEditor = function(image) {
        console.log(image);
        console.log('running ' + image);
        /* 2.a) Prep work for calling `.edit()` */
        function success(newUrl) {
            console.log("Success!", newUrl);
            $state.go('app.add3', {
                'photo': {
                    'afterEditUrl': newUrl,
                    'originalUrl': image
                }
            });

        }

        function error(error) {
            console.log("Error!", error);

            if (error === "Editor Canceled") {
                console.log('goBack');
                $state.go('app.add');

            }
        }

        var imageUrl = "http://167.114.226.27/fbl-2016/201612/175159348.jpg";

        var options = {
            outputType: window.CSDKImageEditor.OutputType.JPEG,
            tools: [
                window.CSDKImageEditor.ToolType.EFFECTS,
                window.CSDKImageEditor.ToolType.CROP,
                window.CSDKImageEditor.ToolType.ADJUST,
                window.CSDKImageEditor.ToolType.ORIENTATION,
                window.CSDKImageEditor.ToolType.LIGHTING,
                window.CSDKImageEditor.ToolType.COLOR,
                window.CSDKImageEditor.ToolType.SHARPNESS,
                window.CSDKImageEditor.ToolType.WHITEN,
                window.CSDKImageEditor.ToolType.DRAW,
                window.CSDKImageEditor.ToolType.MEME,
                window.CSDKImageEditor.ToolType.ENHANCE,
                window.CSDKImageEditor.ToolType.FOCUS,
                window.CSDKImageEditor.ToolType.VIGNETTE,
                window.CSDKImageEditor.ToolType.OVERLAYS

            ],
            quality: 95
        };

        /* 2.b) Launch the Image Editor */
        window.CSDKImageEditor.edit(success, error, image, options);
    };

    $scope.runEditor($stateParams.photo.url);
})

.controller('AddCtrl3', function($scope, $state, $stateParams, $ionicHistory, $cordovaFileTransfer, UserService, HOST) {
    $scope.post = {};
    console.log($state, $stateParams);
    var backView = $ionicHistory.backView();
    backView.stateParams = {
        photo: {
            url: $stateParams.photo.originalUrl
        }
    };
    backView.stateId = backView.stateName + "_url=" + $stateParams.photo.originalUrl;
    $scope.previewPhoto = $stateParams.photo.afterEditUrl;

    $scope.uploadPhoto = function() {
        console.log($scope.post);
        var options = {};
        options.params = {
            title: $scope.post.title,
            note: $scope.post.note
        };
        options.fileKey = 'photo';

        options.headers = {};
        options.headers['Authorization'] = "Bearer " + UserService.getAuthToken();


        document.addEventListener('deviceready', function() {

            $cordovaFileTransfer.upload(encodeURI(HOST + 'upload'), $stateParams.photo.afterEditUrl, options)
                .then(function(result) {
                    console.log('success' + result);
                }, function(err) {
                    console.log('error', err);
                }, function(progress) {
                    console.log((progress.loaded / progress.total) * 100);
                    // constant progress updates
                });

        }, false);

    };

});

/*
POST na
/upload

photo

title
note

[12:05]
ERR_WRONG_FILE_TYPE - 'przekazano błędny typ pliku'
ERR_EMPTY_UPLOAD_FILE - 'przekazano pusty plik'
ERR_WRONG_FILE_MIME_TYPE - 'przekazano błędny plik'
ERR_USER_SUSPENDED - 'uzytkownik jest zablokowany'
ERR_GENERAL_ADD_ERROR - 'błąd przy tworzeniu wpisu o zdjęciu'
*/