angular.module('your_app_name.app.controllers')
    .controller('FeedCtrl', function($scope, $rootScope, $ionicTabsDelegate, $localStorage,
        PostService, FeedService, LikeService, $ionicPopup,
        $state, cacheSrcStorage, $timeout, TABS) {

        $scope.page = 1;
        $scope.feed = true;
        $scope.posts = FeedService.posts;
        $scope.loadingPosts = false;
        $scope.$on("$ionicView.enter", function() {
            $scope.vm.selector = '.feed-view .item-image img';
        });
        $scope.vm.selector = '.feed-view .item-image img';
        $scope.doRefresh = function() {
          $scope.page = 1;
            FeedService.getFeed($scope.page);
            $scope.page++;
        };

        $scope.getNewData = function() {
          $scope.page = 1;
            FeedService.getFeed($scope.page);
            $scope.page++;
         };

        $scope.scrollFeed = function() {
          console.log('scrollFeed $scope.page', $scope.page);
            if ( $scope.loadingPosts ) {
                return;
            }
            $scope.loadingPosts = true;
            $scope.page += 1;
            FeedService.updatePosts($scope.page).then(function(){
            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.loadingPosts = false;
          });
        };

        $scope.moreDataCanBeLoaded = function() {
            return FeedService.totalPages > $scope.page;
        };

        if ($state.current.name === 'app.' + TABS[0].name) {
            $rootScope.$on('refreshAfterError', $scope.getNewData);
        }

        $scope.doRefresh(1);
    });
