angular.module('your_app_name.app.controllers')
    .controller('ExploreCtrl', function($scope, $rootScope, exploreService) {
      $rootScope.$on('$destroy', function(){$scope.$destroy();});

        $scope.page = 1;
        $scope.list = [];
        $scope.getList = function(whichData){
          $scope.page = 1;
          $scope.whichData = whichData;
          $scope.list = [];
          $scope.getData();
          $scope.$broadcast('scroll.refreshComplete');
        };

        $scope.getData = function() {
            exploreService.getData($scope.whichData, $scope.page).then(
                function(res) {
                  console.log(res);
                    $scope.list = $scope.list.concat(res);
                    console.log($scope.list);
                    $scope.page++;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }
            );
        };

       $scope.getList('latest');

    });
