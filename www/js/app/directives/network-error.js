angular.module('your_app_name.common.directives')
.directive('networkError', function ($timeout, cfpLoadingBar) {
    return {
        restrict: 'E',
        scope: true,
        link: function ($rootScope, elem, attr) {
            $rootScope.$on('httpError', function (event, args) {

                if (args.status !== 0) {
                    elem.text('Błąd serwera. Status:  ' + args.status);
                } else {
                    elem.text('Brak połączenia z internetem');
                }
                elem.addClass('showAnim');
                if (args.action) {
                    elem.append('. Odświez.');
                    elem.unbind('click');
                    elem.bind('click', function (e) {
                        $rootScope.$emit('refreshAfterError');
                        console.log('refreszAfterError');
                        elem.removeClass('showAnim');
                    });
                }

            });
            $rootScope.$on('$stateChangeStart', function () {
                elem.removeClass('showAnim');
                cfpLoadingBar.complete();
            });
        }
    };
});
