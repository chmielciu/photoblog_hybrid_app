angular.module('your_app_name.common.directives')
.directive('ngSparkline', function() {
    return {
        restrict: 'A',
        template: '<div class="sparkline"></div>'
    };
});