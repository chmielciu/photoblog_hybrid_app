angular.module('your_app_name.common.directives')
.directive('bg', function() {
    return {
        restrict: 'A',
        require: '^multiBg',
        scope: {
            ngSrc: '@'
        },
        link: function(scope, element, attr, multiBgController) {
            element.on('load', function() {
                multiBgController.animateBg();
            });
        }
    };
});