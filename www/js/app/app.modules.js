/**
 * Inicializacja poszczegolnych modulow, tu mozna dodawac configi
 * czy moduly zalezne
 */
angular.module('your_app_name.app.controllers', []);
angular.module('your_app_name.common.directives', []);
angular.module('your_app_name.app.services', []);
angular.module('your_app_name.app.filters', []);
angular.module('your_app_name.app.constants', []);