# Photoblog.pl ionic hybrid app

This is a ios and android mobile app for photoblog.pl - polish social media photo sharing website. App UX and UI is structured quite similarly to Instagram app with few additions.

Please note, that the project hasn't been finished (i'd say it is 90% complete) and never released. 
Thus you may find some commented out code, console.logs and left overs. Sorry about that.

Main priority was the performance, which is a big challange in hybrid apps. Especially on ios.

When number of elements on the screen increases, animation and scrolling can get laggy and slow.

To prevent this, few unorthodox approaches had to be taken.

I couldn't find any clear guides to make an app this complex perform as native one, so the creation process was very often based on trial and error. The quality of code suffer form this in few places.



For instance, on ios, default javascript interpreting and html renderering is done by wkwebview engine, which is significantly faster then the default one, but it complicates access to local files on a device (native proxy webserver has to be used). 

Also worth noting is that photoblog.pl is a project which has been started in 2003 so it suffers from few past technological limitations.

This has an impact on the REST api which was used for comunication with the backend. This led to few overcomplicated services in the app which parse the results and make them useful for viewing.

There is a lot of localstorage usage. In feed (which is a first tab with list of photos of people you fallow) the list is generated from localstoarge before it queries API. 

Thanks to this waiting for new content is less frustrating.

Due to time pressure unit tests were abandoned.


# Technology used 

  - Ionic  (when I started coding it angular2 was in early beta and ionic v2 wasn't available)
  - Apache Cordova (plenty of native plugins were used, ie. photo editor adobe creative sdk)

# File and directory structure

Main ionic app is in /www/js/app/

Bower modules are in /www/lib/

Node modules are in stanard /node_modules/

/www/css/ holds css generated from /scss/ directory

Html views and partials are in  /www/views/app/



# Ionic app

I started an app with the seed template, which - as I initially thought - would make things easier.

In the end, most of the template code was completely rewriten. 


For instance, navigation of the app was changed from sidebar menu to tabs. 

I really liked the Instagram approach so I decided to go with the tabs. 

Default ionic tabs module is very limited and it doesn't let you to have the same view in different tabs.

It has to be declared as a different state. This is why in www\js\app\constants\states.js are defined states and rules which views can be children of which tabs and states are declared dynamically in main app.js. 

It is useful in situations when for instance you navigate to a single photo view in notification tab and have single photo view with a different photo in a main feed tab.

# Running the app

Steps to be taken in order to run the project in the browser:

```sh
npm install
```
```sh
bower install
```
```sh
ionic serve
```

Sadly the backend is not available anymore, so you won't be able to get past the login screen.


Ps. declaring angular app as 'your_app_name' was an inside joke. But I won't be surprised if you don't find it funny :)